package edu.au.u5913873.cardless_1

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.PersistableBundle
import android.provider.MediaStore
import android.util.Log
import com.bumptech.glide.Glide
import com.microsoft.projectoxford.face.FaceServiceRestClient
import com.microsoft.projectoxford.face.contract.Face
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.*


class MainActivity : AppCompatActivity() {

    private val REQUEST_TAKE_PHOTO = 0
    private val REQUEST_SELECT_IMAGE_IN_ALBUM = 1

    private var counter = 0

    private var uniformFaceID: String = ""

    private var proctorList: MutableList<Proctor>? = null

    private var mUriPhotoTaken: Uri? = null

    private var image: Bitmap? = null

    private var currentImageId: String = ""

    private val retrofitStudent = Retrofit.Builder()
            .baseUrl("http://52.221.98.249:81")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    private val retrofitFaceID = Retrofit.Builder()
            .baseUrl("https://eastasia.api.cognitive.microsoft.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getProctorList("580268", "2", "2017")

        take_photo_btn.setOnClickListener { takePhoto() }


    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState!!.putParcelable("ImageUri", mUriPhotoTaken)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        mUriPhotoTaken = savedInstanceState!!.getParcelable("ImageUri")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_TAKE_PHOTO, REQUEST_SELECT_IMAGE_IN_ALBUM -> if (resultCode == Activity.RESULT_OK) {
                val imageUri: Uri?
                if (data == null || data.data == null) {
                    imageUri = mUriPhotoTaken
                } else {
                    imageUri = data.data
                }
                val intent = Intent()
                intent.data = imageUri
                setResult(Activity.RESULT_OK, intent)
                current_photo.setImageURI(mUriPhotoTaken)
                image = MediaStore.Images.Media.getBitmap(this.contentResolver, imageUri)
                current_photo.setImageBitmap(image)
                //

                previous_student_btn.setOnClickListener { detectCurrentFace(image!!, 0) }

            }
            else -> {
            }
        }
    }

    private fun takePhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (intent.resolveActivity(packageManager) != null) {
            val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)

            try {
                var file = File.createTempFile("IMG_", ".jpg", storageDir)
                mUriPhotoTaken = Uri.fromFile(file)
//                currentPhoto = MediaStore.Images.Media.getBitmap(this.contentResolver, mUriPhotoTaken)
//                imageFile = file
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mUriPhotoTaken)
                startActivityForResult(intent, REQUEST_TAKE_PHOTO)
            } catch (e: IOException) {
                Log.d("Camera", e.message)
                toast("Error: ${e.message}")
            }
        }
    }


    private fun getProctorList(instructorCode: String, semester: String, year: String) {
        retrofitStudent.create(CardlessService::class.java).getProctorList(instructorCode, year, semester)
                .enqueue(object : Callback<MutableList<Proctor>> {
                    override fun onFailure(call: Call<MutableList<Proctor>>?, t: Throwable?) {
                        toast("Error Retrofitting")
                        Log.d("Get Proctor Error", t.toString())
                    }

                    override fun onResponse(call: Call<MutableList<Proctor>>?, response: Response<MutableList<Proctor>>?) {
                        toast("OK!!")
                        Log.d("Get Proctor OK", response!!.code().toString())
                        if (response.body() != null) {
                            // DATA IN
                            proctorList = response.body()
                            displayStudentInformation()
                        }

                    }

                })
    }

    private fun detectUniformPhoto(imageUrl: String) {
        val returnFaceID = true
        val returnFaceLandMarks = false
        val image = ImageUrl(imageUrl)

        retrofitFaceID.create(CardlessService::class.java).detectUniformImage(image, returnFaceID, returnFaceLandMarks)
                .enqueue(object :Callback<MutableList<DetectionResult>> {
                    override fun onFailure(call: Call<MutableList<DetectionResult>>?, t: Throwable?) {
                        toast("Student ID Error!")
                        Log.d("Detect Uniform", t.toString())
                    }

                    override fun onResponse(call: Call<MutableList<DetectionResult>>?, response: Response<MutableList<DetectionResult>>?) {
                        toast("Uniform OK")
                        Log.d("Uniform OK", response!!.body().toString())

                        if (response.body() != null) {
                            uniformFaceID = response.body()!![0].faceId
                        }

                    }

                })
    }

    private fun detectCurrentFace(image: Bitmap, index: Int) {
        val output = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, output)
        val input = ByteArrayInputStream(output.toByteArray())

        DetectionTask().execute(input)
    }

    private fun displayStudentInformation() {
        val studentId = proctorList!![0].StudentList[counter].StudentCode
        val imageUrl = "http://auspark4.cloudapp.net:83/student-picture/normal-size/$studentId.jpg"
        detectUniformPhoto(imageUrl)
        student_id_lbl.text = studentId
        first_name_lbl.text = proctorList!![0].StudentList[counter].FirstName
        last_name_lbl.text = proctorList!![0].StudentList[counter].LastName
        seat_number_lbl.text = proctorList!![0].StudentList[counter].SeatNumber
        Glide.with(this).load(imageUrl).into(uniform_id)
        counter++
    }


    private inner class DetectionTask: AsyncTask<InputStream, String, Array<Face>?>() {
        private var mSucceed = true

        override fun doInBackground(vararg params: InputStream): Array<Face>? {
            // Get an instance of face service client to detect faces in image.
            val faceServiceClient = FaceServiceRestClient(getString(R.string.endpoint), getString(R.string.subscription_key))
            try {
//                publishProgress("Detecting...")

                // Start detection.
                return faceServiceClient!!.detect(
                        params[0], /* Input stream of image to detect */
                        true, /* Whether to return face ID */
                        false, /* Whether to return face landmarks */
                        /* Which face attributes to analyze, currently we support:
                           age,gender,headPose,smile,facialHair */
                        null)
            } catch (e: Exception) {
                mSucceed = false
//                publishProgress(e.message)
                Log.d("Debug", e.message.toString())
                return null
            }

        }

        override fun onPreExecute() {

//            progressDialog.show()
//            Log.d("Detecting in image", mIndex.toString())
        }

        override fun onProgressUpdate(vararg progress: String) {
//            progressDialog.setMessage(progress[0])
//            setInfo(progress[0])
        }

        override fun onPostExecute(result: Array<Face>?) {
            // Show the result on screen when detection is done.
//            Log.d("Momo", result.toString())
//            setUiAfterDetection(result, mIndex, mSucceed)
            currentImageId = result!![0].faceId.toString()
//            current_face_id.text = currentImageId
//            verify_btn.setOnClickListener { verifyFaces(uniformImageId!!, currentImageId!!) }
            Log.d("FACEEEEE", currentImageId)
        }
    }


}
