package edu.au.u5913873.cardless_1

import retrofit2.Call
import retrofit2.http.*

/**
 * Created by Momo on 8/3/2018 AD.
 */
interface CardlessService {

    @GET("exam/proctor/getproctorlist")
    fun getProctorList(@Query("instructorCode") instructorCode: String,
                       @Query("year") year: String,
                       @Query("semester") semester: String) : Call<MutableList<Proctor>>


    @Headers(
            "Content-Type: application/json",
            "Ocp-Apim-Subscription-Key: 5734104ed4f6430c857fd9898d46f2b4"
    )
    @POST("face/v1.0/detect")
    fun detectUniformImage(@Body url: ImageUrl,
                           @Query("returnFaceId") returnFaceId: Boolean,
                           @Query("returnFaceLandmarks") returnFaceLandMarks: Boolean) : Call<MutableList<DetectionResult>>
}