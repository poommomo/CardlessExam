package edu.au.u5913873.cardless_1

/**
 * Created by Momo on 8/3/2018 AD.
 */

data class Proctor (
        var InstructorCode: String,
        var Date: String,
        var Room: String,
        var StudentList: MutableList<StudentList>
)

data class StudentList(
        var StudentCode: String,
        var FirstName: String,
        var LastName: String,
        var FullName: String,
        var ImageUrl: String,
        var SeatNumber: String,
        var SectionNumber: String,
        var IsCheckIn: Boolean,
        var IsAbsent: Boolean
)

data class DetectionResult(var faceId: String)

// ImageURL for POST Detection Service
data class ImageUrl(var url: String)